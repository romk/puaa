# MIT License
#
# Copyright (c) 2019 Roman Kindruk
#
# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in all
# copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
# SOFTWARE.


from flask import Flask
from .puaa import PuaaServer
import os
import yaml


def read_config():
    config_file = os.getenv('UAA_CONFIG_FILE', '/etc/uaa.yml')
    with open(config_file) as cf:
        return yaml.safe_load(cf)


app = Flask(__name__)
srv = PuaaServer(app, read_config())


@app.route('/oauth/token', methods=['POST'], strict_slashes=False)
def issue_token():
    return srv.create_token_response()


@app.route('/token_keys', strict_slashes=False)
def token_keys():
    return srv.token_keys()
